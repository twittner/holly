Holly
=====

A simplistic actor model library using futures.

Documentation
-------------

API documentation is available at https://twittner.gitlab.io/holly/holly/
