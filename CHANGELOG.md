# 0.5.3

- `Scheduler::single_threaded` has been added to support executors which are
not `Sync`. See merge request [#1] for details.

[#1]: https://gitlab.com/twittner/holly/merge_requests/1

# 0.5.2

- Maintenance release without API changes.

# 0.5.1

- Maintenance release without API changes.

# 0.5.0

- Distinguish stoppable and closable streams. To update from previous
  versions replace `holly::stream::stream` with `holly::stream::stoppable`.

# 0.4.0

- Split `io` module into `stream` and `sink`.
- Rename `StreamItem` to `Event` and include the ID in all cases, not
  just `End`.

# 0.3.0

- Changed and simplified `io` module.
- Changed `Actor::process` method to use an optional message where a `None`
  denotes the end of the message stream.
- Actors are now allowed to add additional streams of messages to their
  mailbox (cf. `State::Stream`).
- Actors can close their primary mailbox (cf. `State::Close`).
- Changed license from 0BSD to Blue Oak Model License version 1.0.0
  (see LICENSE.md or https://blueoakcouncil.org/license/1.0.0)

# 0.2.0

- Added `io` module.

# 0.1.0

- Initial release.
