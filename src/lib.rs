//! A simplistic actor model library using futures.

pub mod actor;
pub mod error;
pub mod fold;
pub mod prelude;
pub mod sink;
pub mod stream;

pub use error::Error;
