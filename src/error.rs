use std::{self, fmt};

/// Possible errors when interacting with or creating actors.
#[derive(Debug)]
pub enum Error {
    /// Message recipient does not exist anymore.
    ActorGone,
    /// The recipient's mailbox has reached its limit.
    MailboxFull,
    /// Spawning a new actor failed.
    SpawnError,

    #[doc(hidden)]
    __Nonexhaustive
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::ActorGone => f.write_str("message recipient is gone"),
            Error::MailboxFull => f.write_str("recipient's mailbox is full"),
            Error::SpawnError => f.write_str("failed to spawn actor"),
            _ => f.write_str("__Nonexhaustive")
        }
    }
}

impl std::error::Error for Error {}

